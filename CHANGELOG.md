## 1.1.2 (2024-05-15)
+ 【重要】新增支持 `web` 版本。
+ 优化其他已知问题。

## 1.1.1（2024-05-15）
+ 修复 `srcMode` 为 `update` 情况下第一次单独设置 `src` 索引异常的问题。
+ 优化其他已知问题。

## 1.1.0（2024-05-14）
+ 【重要】支持 `app-ios`。
+ 【重要】新增 `srcMode` 属性，用来约束设置src时的行为。默认为 `push`
	+ push 当前播放列表新增曲目地址
	+ update 更新当前播放曲目地址
+ 【重要】新增 `setPlaybackSrc` 方法。设置 `src` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackStartTime` 方法。设置 `startTime` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackAutoplay` 方法。设置 `autoplay` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackAutonext` 方法。设置 `autonext` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackLoop` 方法。设置 `loop` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `obeyMuteSwitch` 属性。是否遵循系统静音开关，当此参数为 `false` 时，即使用户打开了静音开关，也能继续发出声音，默认值 `true`。仅对 `IOS` 有效。
+ 【重要】新增 `setPlaybackObeyMuteSwitch` 方法。设置 `obeyMuteSwitch` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackVolume` 方法。设置 `volume` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackRates` 方法。设置 `playbackRate` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackCurrentTime` 方法。设置 `currentTime` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `currentIndex` 属性。当前播放的音频索引，设置多个音频时有效，单个音频时该参数默认为 `0`
+ 【重要】新增 `setPlaybackCurrentIndex` 方法。设置 `currentIndex` 。IOS属性设置补丁方案，后面版本会移除
+ 【重要】新增 `setPlaybackSrcMode` 方法。设置 `srcMode` 。IOS属性设置补丁方案，后面版本会移除

## 1.0.0（2024-05-10）
+ 初始发布。
