# kux-audio-player
`kux-audio-player` 是一个参考小程序 [createInnerAudioContext](https://developers.weixin.qq.com/miniprogram/dev/api/media/audio/wx.createInnerAudioContext.html) 设计实现的音频播放插件，方便开发者们简化音频播放实现流程，快速进行业务开发。
> **提示**
> 
> 该插件基于原生插件实现，需要自定义基座方可正常使用。

## 插件特色
+ 完全参考小程序API设计
+ 支持单曲循环，列表循环
+ 支持设置播放列表
+ 支持倍速播放
+ 支持快进、快退等
+ 完整的错误规范设计

## 目录结构
<ol>
	<li><a href="#guide">基础</a>
		<ol>
			<li><a href="#guide_guide">入门使用</a></li>
		</ol>
	</li>
	<li><a href="#api">API</a>
		<ol>
			<li><a href="#api_props">属性</a></li>
			<ol>
				<li><a href="#api_props_src">src</a></li>
				<li><a href="#api_props_startTime">startTime</a></li>
				<li><a href="#api_props_autoplay">autoplay</a></li>
				<li><a href="#api_props_autonext">autonext</a></li>
				<li><a href="#api_props_loop">loop</a></li>
				<li><a href="#api_props_volume">volume</a></li>
				<li><a href="#api_props_playbackRate">playbackRate</a></li>
				<li><a href="#api_props_duration">duration</a></li>
				<li><a href="#api_props_currentTime">currentTime</a></li>
				<li><a href="#api_props_paused">paused</a></li>
				<li><a href="#api_props_buffered">buffered</a></li>
			</ol>
		</ol>
		<ol>
			<li><a href="#api_methods">方法</a></li>
			<ol>
				<li><a href="#api_methods_play">audioPlayer.play()</a></li>
				<li><a href="#api_methods_pause">audioPlayer.pause()</a></li>
				<li><a href="#api_methods_stop">audioPlayer.stop()</a></li>
				<li><a href="#api_methods_seek">audioPlayer.seek()</a></li>
				<li><a href="#api_methods_destroy">audioPlayer.destroy()</a></li>
				<li><a href="#api_methods_onCanplay">audioPlayer.onCanplay()</a></li>
				<li><a href="#api_methods_offCanplay">audioPlayer.offCanplay()</a></li>
				<li><a href="#api_methods_onPlay">audioPlayer.onPlay()</a></li>
				<li><a href="#api_methods_offPlay">audioPlayer.offPlay()</a></li>
				<li><a href="#api_methods_onPause">audioPlayer.onPause()</a></li>
				<li><a href="#api_methods_offPause">audioPlayer.offPause()</a></li>
				<li><a href="#api_methods_onStop">audioPlayer.onStop()</a></li>
				<li><a href="#api_methods_offStop">audioPlayer.offStop()</a></li>
				<li><a href="#api_methods_onEnded">audioPlayer.onEnded()</a></li>
				<li><a href="#api_methods_offEnded">audioPlayer.offEnded()</a></li>
				<li><a href="#api_methods_onTimeUpdate">audioPlayer.onTimeUpdate()</a></li>
				<li><a href="#api_methods_offTimeUpdate">audioPlayer.offTimeUpdate()</a></li>
				<li><a href="#api_methods_onError">audioPlayer.onError()</a></li>
				<li><a href="#api_methods_offError">audioPlayer.offError()</a></li>
				<li><a href="#api_methods_onWaiting">audioPlayer.onWaiting()</a></li>
				<li><a href="#api_methods_offWaiting">audioPlayer.offWaiting()</a></li>
				<li><a href="#api_methods_onSeeking">audioPlayer.onSeeking()</a></li>
				<li><a href="#api_methods_offSeeking">audioPlayer.offSeeking()</a></li>
				<li><a href="#api_methods_onSeeked">audioPlayer.onSeeked()</a></li>
				<li><a href="#api_methods_offSeeked">audioPlayer.offSeeked()</a></li>
				<li><a href="#api_methods_onNext">audioPlayer.onNext()</a></li>
				<li><a href="#api_methods_offNext">audioPlayer.offNext()</a></li>
			</ol>
		</ol>
	</li>
	<li><a href="#format">支持格式</a></li>
	<li><a href="#unierror">错误码</a></li>
	<li><a href="#interface">类型接口</a>
		<ol>
			<li><a href="#interface_OnErrorCallback">OnErrorCallback</a></li>
			<li><a href="#interface_OnCommonCallback">OnCommonCallback</a></li>
			<li><a href="#interface_Loop">Loop</a></li>
			<li><a href="#interface_IAudioPlayer">IAudioPlayer</a></li>
		</ol>
	</li>
</ol>

<a id="guide"></a>
## 基础

<a id="guide_guide"></a>
### 入门使用
下载插件后直接导入页面使用即可，参考下面示例代码：

```
import { createAudioPlayer } from '@/uni_modules/kux-audio-player';

// 创建实例
const audioPlayer = createAudioPlayer();

// 设置音频地址
audioPlayer.src = '/static/ForElise.mp3';

// 错误监听
audioPlayer.onError((error) => {
	console.log(error);
})

// 可以播放状态监听
audioPlayer.onCanplay(() => {
	console.log('可以播放了，当前音频长度：', audioPlayer.duration);
})

// 播放
const play = () => {
	audioPlayer.play();
}

// 暂停
const pause = () => {
	audioPlayer.pause();
}

// 停止播放
const stop = () => {
	audioPlayer.stop();
}

// 释放音频资源
const destroy = () => {
	audioPlayer.destroy();
}
```

> **提示**
> 
> 该插件基于原生插件实现，需要自定义基座方可正常使用。

<a id="api"></a>
## API

<a id="api_props"></a>
### 属性
下面的属性中没有特殊标记 `只读` 的都默认为可读可写属性。
<a id="api_props_src"></a>
#### string src
音频资源的地址，用于直接播放，支持本地地址和网络地址，如果连续设置多次则自动转换为播放列表形式。可以参考下面示例代码。

```
audioPlayer.src = 'https://mp-fb8424b7-d7c9-4520-b348-fd76c6c8607d.cdn.bspapp.com/media/1777051637.mp3';
audioPlayer.src = '/static/ForElise.mp3';
```

<a id="api_props_startTime"></a>
#### number startTime
开始播放的位置（单位：s），默认为 0

<a id="api_props_autoplay"></a>
#### boolean autoplay
是否自动开始播放，默认为 `false`

<a id="api_props_autonext"></a>
#### boolean autonext
是否自动播放下一曲，`src` 设置多次时有效，默认为 `true`

<a id="api_props_loop"></a>
#### Loop loop
循环播放模式，默认为 `off`

+ off 不循环
+ one 单曲循环
+ all 当前列表循环

<a id="api_props_volume"></a>
#### number volume
音量。范围0-1.0。默认为1.0

<a id="api_props_playbackRate"></a>
#### number playbackRate
播放速度。范围 0.5-2.0，默认为 1。（Android 需要 6 及以上版本）

<a id="api_props_duration"></a>
#### number duration `只读`
当前音频的长度（单位 s）。只有在当前有合法的 src 时返回（只读）

<a id="api_props_currentTime"></a>
#### number currentTime
当前音频的播放位置（单位 s）。只有在当前有合法的 src 时返回，时间保留小数点后 6 位，改变 `currentTime` 等同于调用 `seek`。
> **提示**
> 
> 可以在 [onTimeUpdate](#api_methods_onTimeUpdate) 回调函数里面获取该属性实现实时更新播放进度的场景。

<a id="api_props_paused"></a>
#### boolean paused
当前是是否暂停或停止状态，true 表示暂停或停止，false 表示正在播放

<a id="api_props_buffered"></a>
#### number buffered `只读`
音频缓冲的时间点，仅保证当前播放时间点到此时间点内容已缓冲（只读）

<a id="api_methods"></a>
### 方法
下面所有方法的实例名称假设为 `audioPlayer`，实际使用中请替换自己通过 `createAudioPlayer` 创建的实例名称。

<a id="api_methods_play"></a>
#### audioPlayer.play()
播放

<a id="api_methods_pause"></a>
#### audioPlayer.pause()
暂停。暂停后的音频再播放会从暂停处开始播放。

<a id="api_methods_stop"></a>
#### audioPlayer.stop()
停止。停止后的音频再播放会从头开始播放。

<a id="api_methods_seek"></a>
#### audioPlayer.seek(number position)
跳转到指定位置，`position` 单位为s

<a id="api_methods_destroy"></a>
#### audioPlayer.destroy()
销毁当前实例

<a id="api_methods_onCanplay"></a>
#### audioPlayer.onCanplay([OnCommonCallback](#interface_OnCommonCallback) callback)
音频进入可以播放状态，但不保证后面可以流畅播放

<a id="api_methods_offCanplay"></a>
#### audioPlayer.offCanplay()
取消监听 `onCanplay` 事件

<a id="api_methods_onPlay"></a>
#### audioPlayer.onPlay([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频播放事件

<a id="api_methods_offPlay"></a>
#### audioPlayer.offPlay()
移除音频播放事件的监听函数

<a id="api_methods_onPause"></a>
#### audioPlayer.onPause([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频暂停事件

<a id="api_methods_offPause"></a>
#### audioPlayer.offPause()
移除音频暂停事件的监听函数

<a id="api_methods_onStop"></a>
#### audioPlayer.onStop([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频停止事件

<a id="api_methods_offStop"></a>
#### audioPlayer.offStop()
移除音频停止事件的监听函数

<a id="api_methods_onEnded"></a>
#### audioPlayer.onEnded([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频自然播放至结束的事件

<a id="api_methods_offEnded"></a>
#### audioPlayer.offEnded()
移除音频自然播放至结束的事件的监听函数

<a id="api_methods_onTimeUpdate"></a>
#### audioPlayer.onTimeUpdate([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频播放进度更新事件

<a id="api_methods_offTimeUpdate"></a>
#### audioPlayer.offTimeUpdate()
移除音频播放进度更新事件的监听函数

<a id="api_methods_onError"></a>
#### audioPlayer.onError([OnErrorCallback](#interface_OnErrorCallback) callback)
音频播放错误事件

<a id="api_methods_offError"></a>
#### audioPlayer.offError()
取消监听 `onError` 事件

<a id="api_methods_onWaiting"></a>
#### audioPlayer.onWaiting([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频加载中事件。当音频因为数据不足，需要停下来加载时会触发

<a id="api_methods_offWaiting"></a>
#### audioPlayer.offWaiting()
移除音频加载中事件的监听函数

<a id="api_methods_onSeeking"></a>
#### audioPlayer.onSeeking([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频进行跳转操作的事件

<a id="api_methods_offSeeking"></a>
#### audioPlayer.offSeeking()
移除音频进行跳转操作的事件的监听函数

<a id="api_methods_onSeeked"></a>
#### audioPlayer.onSeeked([OnCommonCallback](#interface_OnCommonCallback) callback)
监听音频完成跳转操作的事件

<a id="api_methods_offSeeked"></a>
#### audioPlayer.offSeeked()
移除音频完成跳转操作的事件的监听函数

<a id="api_methods_onNext"></a>
#### audioPlayer.onNext([OnCommonCallback](#interface_OnCommonCallback) callback)
监听即将播放下一曲事件
> **提示**
> 
> 在播放多个曲目并且 [autonext](#api_props_autonext) 为 `true` 时有效。

<a id="api_methods_offNext"></a>
#### audioPlayer.offNext()
移除即将播放下一曲事件的监听函数

<a id="format"></a>
## 支持格式

| 格式 | iOS | Android
| --- | --- | ---
| flac | ✖ | ✅
| m4a | ✖ | ✅
| ogg | ✖ | ✅
| ape | ✖ | ✅
| amr | ✖ | ✅
| wma | ✖ | ✅
| wav | ✖ | ✅
| mp3 | ✖ | ✅
| mp4 | ✖ | ✅
| aac | ✖ | ✅
| aiff | ✖ | ✖
| caf | ✖ | ✖

<a id="unierror"></a>
## 错误码

| errCode | 说明
| --- | ---
| 10001 | 系统错误
| 10002 | 网络错误
| 10003 | 文件错误
| 10004 | 格式错误
| -1 | 未知错误

<a id="interface"></a>
## 类型接口

<a id="OnErrorCallback"></a>
### OnErrorCallback

```
/**
 * 错误回调
 */
export type OnErrorCallback = (error: ApiFail) => void
```

<a id="OnCommonCallback"></a>
### OnCommonCallback

```
/**
 * 公共回调
 */
export type OnCommonCallback = () => void
```

<a id="Loop"></a>
### Loop

```
/**
 * 循环模式
 * + off 不循环
 * + one 单曲循环
 * + all 列表循环
 */
export type Loop = 'off' | 'one' | 'all'
```

<a id="IAudioPlayer"></a>
### IAudioPlayer

```
/**
 * 播放器接口定义
 */
export interface IAudioPlayer {
	/**
	 * 音频资源的地址，用于直接播放，支持本地地址和网络地址，如果连续设置多次则自动转换为播放列表形式。可以参考下面示例代码。
	 * @example
	 * ```
	 * audioPlayer.src = 'https://mp-fb8424b7-d7c9-4520-b348-fd76c6c8607d.cdn.bspapp.com/media/1777051637.mp3';
	 * audioPlayer.src = '/static/ForElise.mp3';
	 *
	 * ```
	 */
	src: string
	/**
	 * 开始播放的位置（单位：s），默认为 0
	 */
	startTime: number
	/**
	 * 是否自动开始播放，默认为 `false`
	 */
	autoplay: boolean
	/**
	 * 是否自动播放下一曲，`src` 设置多次时有效，默认为 `true`
	 */
	autonext: boolean
	/**
	 * 循环播放模式，默认为 `off`
	 * + off 不循环
	 * + one 单曲循环
	 * + all 当前列表循环
	 */
	loop: Loop
	/**
	 * 音量。范围0-1。默认为1
	 */
	volume: number
	/**
	 * 播放速度。范围 0.5-2.0，默认为 1。（Android 需要 6 及以上版本）
	 */
	playbackRate: number
	/**
	 * 当前音频的长度（单位 s）。只有在当前有合法的 src 时返回（只读）
	 */
	readonly duration: number
	/**
	 * 当前音频的播放位置（单位 s）。只有在当前有合法的 src 时返回，时间保留小数点后 6 位，改变 `currentTime` 等同于调用 `seek`。
	 */
	currentTime: number
	/**
	 * 当前是是否暂停或停止状态，true 表示暂停或停止，false 表示正在播放
	 */
	readonly paused: boolean
	/**
	 * 音频缓冲的时间点，仅保证当前播放时间点到此时间点内容已缓冲（只读）
	 */
	readonly buffered: number
	/**
	 * 播放
	 */
	play (): void
	/**
	 * 暂停。暂停后的音频再播放会从暂停处开始播放
	 */
	pause (): void
	/**
	 * 停止。停止后的音频再播放会从头开始播放。
	 */
	stop (): void
	/**
	 * 跳转到指定位置
	 * @param {number} position 需要跳转到的指定位置，单位s
	 */
	seek (position: number): void
	/**
	 * 销毁当前实例
	 */
	destroy (): void
	/**
	 * 音频播放错误事件
	 * @param { OnErrorCallback } 错误事件回调函数
	 */
	onError (callback: OnErrorCallback): void
	/**
	 * 取消监听 `onError` 事件
	 */
	offError (): void
	/**
	 * 音频进入可以播放状态，但不保证后面可以流畅播放
	 */
	onCanplay (callback: OnCommonCallback): void
	/**
	 * 取消监听 `onCanplay` 事件
	 */
	offCanplay (): void
	/**
	 * 监听音频播放事件
	 */
	onPlay (callback: OnCommonCallback): void
	/**
	 * 移除音频播放事件的监听函数
	 */
	offPlay (): void
	/**
	 * 监听音频暂停事件
	 */
	onPause (callback: OnCommonCallback): void
	/**
	 * 移除音频暂停事件的监听函数
	 */
	offPause (): void
	/**
	 * 监听音频停止事件
	 */
	onStop (callback: OnCommonCallback): void
	/**
	 * 移除音频停止事件的监听函数
	 */
	offStop (): void
	/**
	 * 监听音频自然播放至结束的事件
	 */
	onEnded (callback: OnCommonCallback): void
	/**
	 * 移除音频自然播放至结束的事件的监听函数
	 */
	offEnded (): void
	/**
	 * 监听音频播放进度更新事件
	 */
	onTimeUpdate (callback: OnCommonCallback): void
	/**
	 * 移除音频播放进度更新事件的监听函数
	 */
	offTimeUpdate (): void
	/**
	 * 监听音频加载中事件。当音频因为数据不足，需要停下来加载时会触发
	 */
	onWaiting (callback: OnCommonCallback): void
	/**
	 * 移除音频加载中事件的监听函数
	 */
	offWaiting (): void
	/**
	 * 监听音频进行跳转操作的事件
	 */
	onSeeking (callback: OnCommonCallback): void
	/**
	 * 移除音频进行跳转操作的事件的监听函数
	 */
	offSeeking (): void
	/**
	 * 监听音频完成跳转操作的事件
	 */
	onSeeked (callback: OnCommonCallback): void
	/**
	 * 移除音频完成跳转操作的事件的监听函数
	 */
	offSeeked (): void
	/**
	 * 监听即将播放下一曲事件
	 */
	onNext (callback: OnCommonCallback): void
	/**
	 * 移除即将播放下一曲事件的监听函数
	 */
	offNext (): void
}
```

---

### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手

